#!/usr/bin/ksh
#=================================================================================#
# Script:       cp2shelf v1.0                                                     #
# Function:     This script aims to automate the copying of files to target shelf #
#               and generates a report with files' versions and timestamps.       #
# Author:       John Paul P. Doria                                                #
# Date Created: June 5, 2013                                                      #
#=================================================================================#

user=`who am i | awk '{print $1}`
date=`date +%m%d%y`
time=`date +%H%M%S`
cdate=${date}${time}
log=/home/${user}/${user}_elements_${cdate}.log
branch=`cleartool catcs | grep PROMOTE | awk '{print $3}'`

trap "rm -f /home/${user}/${user}_copy2shelf_${cdate}.csv /home/${user}/${user}_${cdate}.mail /home/${user}/${user}_lslrt_${cdate}.log /home/${user}/${user}_ctls_${cdate}.log $log > /dev/null 2>&1" 1 2 3 15

cleanup() {
	rm -f /home/${user}/${user}_copy2shelf_${cdate}.csv /home/${user}/${user}_${cdate}.mail /home/${user}/${user}_lslrt_${cdate}.log /home/${user}/${user}_c
tls_${cdate}.log $log > /dev/null 2>&1
}

main() {
clear
echo "============================"
echo " COPY ELEMENTS TO SHELF DIR "
echo "============================"
echo "Target Directory: \c"
read targetdir
echo "VOB: \c"
read vob
echo "Email Address: \c"
read email
vi $log
confirmation
}

confirmation() {
clear
echo "========================="
echo " C O N F I R M A T I O N "
echo "========================="
echo "Target Directory: $targetdir"
echo "VOB: /ccvobs/$vob"
echo "Email Address: $email"
echo
echo "--------------------------------"
echo " List of elements to be copied: "
echo "--------------------------------"
cat $log
echo
echo "Press Enter to proceed or Q to quit. \c"
read choice

choice=`echo $choice | tr [A-Z] [a-z]`
if [ -z $choice ]; then
        clear
	for elem in `cat $log`; do
		if [ `find $targetdir -name "$elem" | wc -l` -eq 1 ]; then
			echo "Error: duplicates found in $targetdir. Please clean the directory and try again."
			sleep 2
			cleanup
			exit 1
		else
		        findelem
			break
		fi
	done
        ctls
        lslrt
        emailreport
elif [ $choice == "q" ]; then
	rm -f $log
        clear
        exit 0
fi
}

findelem() {
echo "Finding elements..."
for a in `cat $log`; do
	if [ `find /ccvobs/$vob -name "$a" | wc -l` -eq 1 ]; then
		find /ccvobs/$vob -type f -name "$a" -exec cp -p {} $targetdir \;
		echo "$a -> $targetdir."
	elif [ `find /ccvobs/$vob -name "$a" | wc -l` -gt 1 ]; then
		echo "\033[1m\033[5mUnable to copy $a!\033[0m\033[0m"
	elif [ `find /ccvobs/$vob -name "$a" | wc -l` -eq 0 ]; then
		echo "\033[1m\033[5m$a is not found!\033[0m\033[0m"
	fi
done
}

ctls() {
echo "Creating ${user}_copy2shelf.csv..."
echo "Element(s)" >> /home/${user}/${user}_ctls_${cdate}.log
for b in `cat $log`; do
	if [ `find /ccvobs/$vob -name "$b" | wc -l` -eq 1 ]; then
		find /ccvobs/$vob -type f -name "$b" -exec cleartool ls -s {} \; >> /home/${user}/${user}_ctls_${cdate}.log
	fi
done
}

lslrt() {
echo "Timestamp(s)" >> /home/${user}/${user}_lslrt_${cdate}.log
for c in `cat $log`; do
	if [ `find /ccvobs/$vob -name "$c" | wc -l` -eq 1 ]; then
		find /ccvobs/$vob -type f -name "$c" -exec ls -lrt {} \; | awk '{print $6,$7,$8}' >> /home/${user}/${user}_lslrt_${cdate}.log
	fi
done
}

emailreport() {
echo "Sending report to $email..."
echo "This is an automated email. Please do not reply." >> /home/${user}/${user}_${cdate}.mail
echo "Attached here is the log generated after copying the files to $targetdir." >> /home/${user}/${user}_${cdate}.mail

echo "List of files copied to $targetdir" >> /home/${user}/${user}_copy2shelf_${cdate}.csv
echo "Branch: $branch" >> /home/${user}/${user}_copy2shelf_${cdate}.csv
echo "Report Date: `date`\n" >> /home/${user}/${user}_copy2shelf_${cdate}.csv
paste -d, /home/${user}/${user}_ctls_${cdate}.log /home/${user}/${user}_lslrt_${cdate}.log >> /home/${user}/${user}_copy2shelf_${cdate}.csv

(cat /home/${user}/${user}_${cdate}.mail; uuencode /home/${user}/${user}_copy2shelf_${cdate}.csv ${user}_copy2shelf_${cdate}.csv) | mailx -s "${branch}: copy to shelf is complete!" -r "vobadm" "$email"

sleep 3
rm -f /home/${user}/${user}_copy2shelf_${cdate}.csv /home/${user}/${user}_${cdate}.mail /home/${user}/${user}_lslrt_${cdate}.log /home/${user}/${user}_ctls_${cdate}.log $log > /dev/null 2>&1

echo "Finished."
}

viewcheck() {
if [ `cleartool pwv -s | grep -c NONE` -eq 1 ]; then
        echo "cleartool: Error: Please make sure you are using a view."
	exit 1
fi
}

createlog() {
echo "You are in vi text editor. Please delete this line then insert the list of elements here." >> $log
}

createlog
viewcheck
main
exit 0
