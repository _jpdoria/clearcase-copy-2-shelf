# About #
This script automates the copying of files to target shelf and generates a report with files' versions and timestamps.

# Usage #
```
#!bash
./cp2shelf.ksh
```
